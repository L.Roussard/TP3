// Port to listen requests from
var port = 1234;

// Modules to be used
var path = require('path');
var express = require("express");
var bodyParser = require("body-parser");
var cookieParser = require("cookie-parser");
var sqlite3 = require("sqlite3").verbose();
var app = express();
var db = new sqlite3.Database("db.sqlite");
var shajs = require("sha.js");
// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Log requests
app.all("*", urlencodedParser, function (req, res, next) {
  console.log(req.method + " " + req.url);
  console.dir(req.headers);
  console.log(req.body);
  console.log();
  next();
});

// Serve static files
app.use(express.static("public"));
app.use(cookieParser());

app.get("/users", function (req, res, next) {
  db.all("SELECT rowid, ident, password FROM users;", function (err, data) {
    res.json(data);
  });
});

app.get("/home", function (req, res, next) {
  verificationCookie(req.cookies.token, res);
});

app.post("/login", function (req, res, next) {
  var log = req.body.login; var mdp = req.body.pswd;
  authentification(log, mdp, res);
});

app.post("/logout", function (req, res, next) {
  var log = req.body.login; var mdp = req.body.pswd;
  deconnexion(req.cookies.token, res);
});



// Startup server
app.listen(port, function () {
  console.log("Le serveur est accessible sur http://localhost:" + port + "/");
  console.log("La liste des utilisateurs est accessible sur http://localhost:" + port + "/users");
});

function authentification(log, mdp, res) {
  checkLogin(log, mdp, res)
  function checkLogin(log, mdp, res) {
      db.all("SELECT ident, password FROM users WHERE ident=? AND password=?;", [log, mdp],
        function (err, data) {
          if (data) {
            var token = createToken(log, mdp);
            saveToken(log, token);
            res.cookie("token", token).redirect('/home');
          }          
          else
            res.redirect('/');
        });
  }
}

function verificationCookie(t, res) {
  db.all("SELECT token FROM sessions WHERE token=?;", [t],
    function (err, data) {
      if (data && data.length > 0)
        res.redirect('/home.html');
      else
        res.redirect('/');
    });
}

function deconnexion(t, res) {
  db.all("DELETE FROM sessions WHERE token=?;", [t],
    function (err, data) {
        res.redirect('/');
    });
}

function createToken(log, mdp) {
  // Creation du token
  var rand = function () {
    return Math.random()
      .toString(36)
      .substr(2)  // remove `0.`
  };
  var t = rand() + shajs("sha256").update(log + new Date().getTime().toString() + mdp).digest("hex");

  return t;
}

function saveToken(log, t) {
    db.all('SELECT * FROM sessions WHERE ident=?;', [log], function (err, data) {
      // Update du token 
      if (data.length > 0) {
        db.run("INSERT INTO sessions (ident, token) VALUES (?, ?);", [log, t],
          function (err, data) {
          if (data)
            return true;
          else
            return false;
          });
      }
      else {
        db.run('UPDATE sessions SET token=? WHERE ident=?;', [t, log],
          function (err, data) {
          if (data)
            return true;
          else
            return false;
          });
      }
    });
}

